# Documentation for developers

See [CONTRIBUTING](CONTRIBUTING.md)

# Documentation for EvtGen admins

* Guide to [making a new release](dev-docs/MakeRelease.md)

