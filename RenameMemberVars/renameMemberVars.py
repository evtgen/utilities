#!/usr/bin/env python3

'''
Read the various html/functions_vars*.html files created by Doxygen and from
them determine a list of member variables for each class in EvtGen that do not
match the m_varName convention.
Construct and execute a sed command to rename them in both .hh and .cpp files.

Entries in those files look like the following:
<li>beta2
: <a class="el" href="classEvtVubBLNP.html#a29cae36f7579abb3f22fac40d04114ae">EvtVubBLNP</a>
, <a class="el" href="classEvtVubBLNPHybrid.html#a948956bfcc3af4bd0897190a23e9afda">EvtVubBLNPHybrid</a>
</li>
'''

import os
import re
import sys
from subprocess import Popen, PIPE

class MemberVarConventionChecker :
    '''
    Class that can check and enforce naming convention for class member variables.
    The convention is:
    * m_ prefix
    * remainder in either UpperCamelCase or lowerCamelCase (not yet checked or enforced)
    * no trailing underscore
    e.g.
    m_varName
    '''


    def __init__( self, sourceBaseDir, doxygenBaseDir ) :
        '''
        Constructor
        Required arguments:
        sourceBaseDir  : the directory where the EvtGen sources are located, i.e. that containing the EvtGen top-level CMakeLists.txt file
        doxygenBaseDir : the directory where the EvtGen Doxygen output (generated from the same sources!) is located, i.e. that containing the index.html file
        '''

        # assign from arguments
        self.sourceBaseDir = sourceBaseDir
        self.doxygenBaseDir = doxygenBaseDir

        # initialise everything else we need
        self.memberListFiles = []
        self.classToVars = {}

        # initial actions
        self.checkDirectories()
        self.constructListOfAllMemberVars()
        self.filterOutConformingVars()


    def checkDirectories( self ) :
        '''
        Check that the directories with which we've been supplied contain the expected files/directories
        The source directory should contain EvtGen, EvtGenBase, EvtGenModels, EvtGenExternal, src directories
        The doxygen directory should contain index.html etc.
        '''

        sourceDirListing = os.listdir( self.sourceBaseDir )
        expectedDirs = [ 'EvtGen', 'EvtGenBase', 'EvtGenModels', 'EvtGenExternal', 'src' ]
        for expectedDir in expectedDirs :
            if expectedDir not in sourceDirListing :
                raise Exception('Problem with supplied source directory - it does not contain the correct directories: EvtGen, EvtGenBase, EvtGenModels, EvtGenExternal, src')

        doxygenDirListing = os.listdir( self.doxygenBaseDir )
        if 'index.html' not in doxygenDirListing :
            raise Exception('Problem with supplied Doxygen directory - it does not contain an index.html file')

        self.findMemberListFiles()


    def findMemberListFiles( self ) :
        '''
        Fill the list of Doxygen files that list the member variables:
        functions_vars*.html
        '''

        self.memberListFiles = []

        findCmd = f'find {self.doxygenBaseDir} -name functions_vars*.html'
        with open('/dev/null') as serr :
            pipe = Popen( findCmd.split(),
                        env = os.environ ,
                        stdout = PIPE    ,
                        stderr = serr    )

            stdout = pipe.stdout
            for line in stdout :
                self.memberListFiles.append(line.decode( sys.stdout.encoding ).rstrip())


    def findClassSourceFiles( self, className ) :
        '''
        From the Doxygen files that document each class:
        classClassName.html
        determine the corresponding source file(s)
        '''

        sourceFiles = []

        doxygenFileName = f'class{className}.html'
        if '_' in doxygenFileName :
            doxygenFileName = doxygenFileName.replace( '_', '__' )
        if '::' in doxygenFileName :
            doxygenFileName = doxygenFileName.replace( '::', '_1_1' )

        try :
            with open( f'{self.doxygenBaseDir}/{doxygenFileName}' ) as doxygenFile :
                lines = doxygenFile.readlines()
        except :
            doxygenFileName = f'struct{doxygenFileName[5:]}'
            with open( f'{self.doxygenBaseDir}/{doxygenFileName}' ) as doxygenFile :
                lines = doxygenFile.readlines()

        startLine = -1
        endLine = -1
        for lineNo, line in enumerate(lines) :
            if re.search( 'was generated from the following file', line ) :
                startLine = lineNo + 1
            elif startLine != -1 and re.search( '^</ul>', line ) :
                endLine = lineNo - 1
                break
        else :
            raise Exception(f'Could not locate source files for {className}')

        for lineNo in range( startLine, endLine+1 ) :
            mymatch = re.search( '^<li>([\w/]+)<.*>([\w.]+)</a></li>$', lines[lineNo] )
            if not mymatch :
                continue
            sourceFiles.append( mymatch.groups()[0] + mymatch.groups()[1] )

        return sourceFiles


    def constructListOfAllMemberVars( self ) :
        '''
        Read the Doxygen files to construct a dictionary that associates each class/struct with a list of its member variables
        '''

        self.classToVars = {}

        # loop through the input files
        for inputfilename in self.memberListFiles :

            # read the file
            with open( inputfilename ) as inputfile :
                lines = inputfile.readlines()

            liStartLines = []
            liEndLines = []
            varNames = []

            # loop through each line to locate the start and end of each 'list item'
            # tag and the variable name, which comes at the end of the first line
            for lineNo, line in enumerate(lines) :
                if re.search( '^<li>', line ) :
                    liStartLines.append( lineNo )
                    varname = line[4:].rstrip()
                    varNames.append( varname )
                elif re.search( '^</li>', line ) :
                    liEndLines.append( lineNo )

            # loop through the entries and for each one inspect the intervening lines
            # to extract the names of the classes
            for varName, startLine, endLine in zip(varNames, liStartLines, liEndLines) :
                for line in lines[startLine+1:endLine] :

                    mymatch = re.search( '>([:\w]+)[<&]', line )
                    if ( mymatch ) :
                        className = mymatch.groups()[0]
                    else :
                        line = line.strip()
                        print(f'No class name found in line: "{line}"')
                        continue

                    if className in self.classToVars :
                        self.classToVars[className].append(varName)
                    else :
                        self.classToVars[className] = [varName]

        # remove EvtConst, which we don't want to change
        self.classToVars.pop( 'EvtConst', [] )

        # remove some variables that appear incorrect due to a bug in Doxygen (presumably)
        try :
            self.classToVars['EvtAmplitudeSum'].remove('c')
            self.classToVars['EvtBLLNuLAmp::ResPole'].remove('c')
            self.classToVars['EvtHQETFF'].remove('c')
            self.classToVars['EvtPdfSum'].remove('c')
            self.classToVars['EvtLambda2PPiForLambdaB2LambdaV'].remove('D')
            self.classToVars['EvtWilsonCoefficients'].remove('D')
        except ValueError :
            pass
        except KeyError :
            pass


    def filterOutConformingVars( self ) :
        '''
        Drop variables that already conform to the 'm_varName' style.
        Drop classes where all variables already conform.
        TODO - extend to check for UpperCamelCase or lowerCamelCase compliance
        '''

        classesDone = []
        for className, varList in self.classToVars.items() :
            varsDone = []
            for varName in varList :
                mymatch = re.search( '^m_\w+(?<!_)$', varName )
                if mymatch :
                    varsDone.append( varName )
            for varName in varsDone :
                varList.remove( varName )
            if len(varList) == 0 :
                classesDone.append( className )
        for className in classesDone :
            self.classToVars.pop( className )


    def filterOutSpecificClasses( self, fileName = None, classNamesToSkip = [] ) :
        '''
        Filter from the list of classes those specified either in a file or in a list of strings (or both)
        '''

        if isinstance( fileName, str ) and len(fileName) != 0 :
            with open( fileName ) as skipFile :
                for line in skipFile :
                    line = line.rstrip()
                    if '/' in line :
                        className = line.split('/')[1]
                    else :
                        className = line
                    classNamesToSkip.append( className )

        for className in classNamesToSkip :
            self.classToVars.pop( className, [] )


    def getNonConformingClasses( self ) :
        '''
        Provide access to the dictionary of non-conforming classes and variables
        '''

        return list( self.classToVars.keys() )


    def getNonConformingClassesAndMembers( self ) :
        '''
        Provide access to the dictionary of non-conforming classes and variables
        '''

        return self.classToVars


    def enforceConformance( self, className ) :
        '''
        Change all variable names in the source files of the specified class to
        conform with the defined naming convention
        '''

        if not className in self.classToVars :
            print(f'Class {className} already conforms, nothing to do')
            return

        varList = self.classToVars[className]

        print(f'Modifying {className} variables : {varList}')

        classFiles = self.findClassSourceFiles( className )
        if len(classFiles) == 0 :
            print(f'Cannot find source files for {className}')
            return

        sedCmd = 'sed -i.bak'
        for varName in varList :
            if varName.startswith('_') and varName.endswith('_') :
                sedCmd += f' -e s/\\b{varName}\\b/m{varName[:-1]}/g'
            elif varName.startswith('_') :
                sedCmd += f' -e s/\\b{varName}\\b/m{varName}/g'
            elif varName.endswith('_') :
                sedCmd += f' -e s/\\b{varName}\\b/m_{varName[:-1]}/g'
            else :
                sedCmd += f' -e s/\\b{varName}\\b/m_{varName}/g'
        for sourceFile in classFiles :
            sedCmd += f' {self.sourceBaseDir}/{sourceFile}'

        pipe = Popen( sedCmd.split(),
                    env = os.environ ,
                    stdout = PIPE    ,
                    stderr = PIPE    )

        returncode = pipe.wait()

        if returncode == 0 :
            print(f'Successfully modified {className}')
        else :
            print(f'Problem modifying {className}')
            stdout = pipe.stdout
            stderr = pipe.stderr
            print('\nStdOut was:')
            for line in stdout :
                print( line.decode( sys.stdout.encoding ).rstrip() )
            print('\nStdErr was:')
            for line in stderr :
                print( line.decode( sys.stdout.encoding ).rstrip() )


    def enforceConformanceAll( self ) :
        '''
        For all classes with non-conforming member variable names,
        change those names to conform
        '''

        for className in self.classToVars :
            self.enforceConformance( className )


if '__main__' == __name__ :
    
    nArgs = len(sys.argv)
    if nArgs != 4 and nArgs != 5 :
        print(f'Usage: {sys.argv[0]} <"check" or "enforce"> <EvtGen source dir> <EvtGen Doxygen dir> [skip class filename]')
        sys.exit(2)

    command = sys.argv[1]
    sourceDir = sys.argv[2]
    doxygenDir = sys.argv[3]

    if 'check' not in command and command != 'enforce' :
        print(f'Usage: {sys.argv[0]} <"check" or "enforce"> <EvtGen source dir> <EvtGen Doxygen dir> [skip class filename]')
        sys.exit(2)

    mvc = MemberVarConventionChecker( sourceDir, doxygenDir )
    if nArgs == 5 :
        filterFile = sys.argv[4]
        mvc.filterOutSpecificClasses( fileName = filterFile )

    if 'check' in command :
        if command == 'check-details' :
            badclasses = mvc.getNonConformingClassesAndMembers()
        else :
            badclasses = mvc.getNonConformingClasses()
        if len(badclasses) == 0 :
            print('All classes conform')
            sys.exit(0)
        else :
            print('The following classes do not conform:')
            if command == 'check-details' :
                for badclass in badclasses.items() :
                    className = badclass[0]
                    varNames = badclass[1]
                    fileNames = mvc.findClassSourceFiles( className )
                    print( f'{className} defined in {fileNames}\nhas the following non-conforming variables:\n{varNames}\n' )
            else :
                for badclass in badclasses :
                    print(badclass)
            sys.exit(1)
    elif command == 'enforce' :
        #mvc.enforceConformance( 'EvtGen' )
        mvc.enforceConformanceAll()
        sys.exit(0)


