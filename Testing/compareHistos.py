########################################################################
# Copyright 1998-2024 CERN for the benefit of the EvtGen authors       #
#                                                                      #
# This file is part of EvtGen.                                         #
#                                                                      #
# EvtGen is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# EvtGen is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     #
########################################################################

import sys

nargs = len(sys.argv)
if nargs != 3 and nargs != 4 :
    print(f'Usage: {sys.argv[0]} <ROOT file> <Ref ROOT file> [output dir]')
    sys.exit(1)

newFileName = sys.argv[1]
refFileName = sys.argv[2]
outputDir = "."
if nargs == 4 :
    outputDir = sys.argv[3]

import os

if not os.path.exists( outputDir ) :
    os.mkdir( outputDir )
else :
    outputDir = os.path.realpath( outputDir )
    if not os.path.isdir( outputDir ) :
        print(f'Supplied output dir, "{outputDir}" exists but is not a directory!')
        sys.exit(1)

from ROOT import gROOT, gStyle, TLegend, TCanvas, TFile, TH1D, TH2D, TMath, kRed, kBlue, kViridis

gROOT.SetBatch(True)

newFile = TFile.Open( newFileName )
refFile = TFile.Open( refFileName )

newKeys = newFile.GetListOfKeys()
refKeys = refFile.GetListOfKeys()

gStyle.SetOptStat(0)
gStyle.SetPalette(kViridis)

canvas = TCanvas('c1', 'c1')

def makePullHist( newHist, refHist ) :
    pullHist = newHist.Clone( newHist.GetName() + "_Pull" )
    pullHist.Reset()
    
    nbins = pullHist.GetNcells()
    for ibin in range(nbins) :
        newContent = newHist.GetBinContent(ibin)
        refContent = refHist.GetBinContent(ibin)
        newError = newHist.GetBinError(ibin)
        refError = refHist.GetBinError(ibin)

        pullHist.SetBinError( ibin, 1.0 )

        if newContent == 0.0 and refContent == 0.0 :
            pullHist.SetBinContent( ibin, 0.0 )
            continue

        residual = newContent - refContent
        error = newError if newError != 0.0 else refError
        pull = residual / error

        pullHist.SetBinContent( ibin, pull )

    return pullHist

def checkPullHist( hist ) :
    """
    Checks for essentially 0 pull in every bin
    i.e. checks that original histograms are identical
    """
    nbins = hist.GetNcells()
    for ibin in range(nbins) :
        content = hist.GetBinContent(ibin)
        if abs(content) > 1e-12 :
            return False
    return True

for newKey, refKey in zip( newKeys, refKeys ) :

    if newKey.GetClassName() != refKey.GetClassName() :
        print(f'Mismatch in object type names: {newKey.GetClassName()} vs {refKey.GetClassName()}')
        continue

    if newKey.GetName() != refKey.GetName() :
        print(f'Mismatch in object names: {newKey.GetName()} vs {refKey.GetName()}')
        continue

    newHist = newKey.ReadObj()
    refHist = refKey.ReadObj()

    if newKey.GetClassName() == 'TH1D' :

        newHist.SetLineColor( kBlue )
        refHist.SetLineColor( kRed )

        newHist.Draw()
        refHist.Draw('same')

        legend = TLegend( 0.2, 0.1 )
        legend.AddEntry( newHist, "New", "L" )
        legend.AddEntry( refHist, "Ref", "L" )

        legend.Draw()

        canvas.SaveAs( f'{outputDir}/{newKey.GetName()}.png' )

        pullHist = makePullHist( newHist, refHist )

        pullHist.Draw()

        if not checkPullHist( pullHist ) :
            print(f'Non-zero pulls found in {newKey.GetName()}_Pull')

        canvas.SaveAs( f'{outputDir}/{newKey.GetName()}_Pull.png' )

    elif newKey.GetClassName() == 'TH2D' :

        newHist.Draw("COLZ")

        canvas.SaveAs( f'{outputDir}/{newKey.GetName()}_New.png' )

        refHist.Draw("COLZ")

        canvas.SaveAs( f'{outputDir}/{newKey.GetName()}_Ref.png' )

        pullHist = makePullHist( newHist, refHist )

        pullHist.Draw("COLZ")

        if not checkPullHist( pullHist ) :
            print(f'Non-zero pulls found in {newKey.GetName()}_Pull')

        canvas.SaveAs( f'{outputDir}/{newKey.GetName()}_Pull.png' )

