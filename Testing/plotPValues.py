########################################################################
# Copyright 1998-2024 CERN for the benefit of the EvtGen authors       #
#                                                                      #
# This file is part of EvtGen.                                         #
#                                                                      #
# EvtGen is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# EvtGen is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with EvtGen.  If not, see <https://www.gnu.org/licenses/>.     #
########################################################################

inputFileName = 'p-values.txt'
outputFileName = inputFileName.replace( '.txt', '.png' )

from ROOT import gROOT, gStyle, TLegend, TCanvas, TFile, TH1D, TH2D, kRed, kBlue

gROOT.SetBatch(True)

gStyle.SetOptStat(0)

canvas = TCanvas('c1', 'c1')

hist = TH1D('p_values', '', 100, 0.0, 1.0)

with open(inputFileName) as inputFile :
    line = inputFile.readline()
    while line :
        p_value = float( line )
        hist.Fill( p_value )
        line = inputFile.readline()

hist.Draw()
canvas.SaveAs( outputFileName )
